const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})
describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet a in password', () => {
    expect(checkAlphabet('asfsdf')).toBe(true)
  })
  test('should has alphabet b in password', () => {
    expect(checkAlphabet('bsdfxcv')).toBe(true)
  })
  test('should has alphabet c in password', () => {
    expect(checkAlphabet('asdcv')).toBe(true)
  })
  test('should has alphabet d in password', () => {
    expect(checkAlphabet('afsdfsa')).toBe(true)
  })
  test('should has alphabet e in password', () => {
    expect(checkAlphabet('sdfsew')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet B in password', () => {
    expect(checkAlphabet('BDAWDSAFAsdfsdf')).toBe(true)
  })
  test('should has alphabet C in password', () => {
    expect(checkAlphabet('ADCVFVBFafasdf')).toBe(true)
  })
  test('should has alphabet D in password', () => {
    expect(checkAlphabet('ADAWDDVfsdf@1314')).toBe(true)
  })
  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('111')).toBe(false)
  })
})
describe('Test Digit', () => {
  test('should has digit 0 in password to be true', () => {
    expect(checkDigit('0aSDASdcs')).toBe(true)
  })
  test('should has digit 1 in password to be true', () => {
    expect(checkDigit('1asdaWD!!')).toBe(true)
  })
  test('should has digit 2 in password to be true', () => {
    expect(checkDigit('02AFsfsef!')).toBe(true)
  })
  test('should has digit 3 in password to be true', () => {
    expect(checkDigit('3asdad@!#')).toBe(true)
  })
  test('should has digit 4 in password to be true', () => {
    expect(checkDigit('4asda#')).toBe(true)
  })
  test('should has digit 5 in password to be true', () => {
    expect(checkDigit('5asdwadfgvcb')).toBe(true)
  })
  test('should has digit 6 in password to be true', () => {
    expect(checkDigit('6asdaasd')).toBe(true)
  })
  test('should has digit 7 in password to be true', () => {
    expect(checkDigit('7sdrga')).toBe(true)
  })
  test('should has digit 8 in password to be true', () => {
    expect(checkDigit('8ewqa')).toBe(true)
  })
  test('should has digit 9 in password to be true', () => {
    expect(checkDigit('9a22')).toBe(true)
  })
  test('should has no digit in password', () => {
    expect(checkDigit('aasc@')).toBe(false)
  })
})
describe('Test Symbol', () => {
  test('should has symbol ! in password to be true', () => {
    expect(checkSymbol('111!')).toBe(true)
  })
  test('should has symbol ~ in password to be true', () => {
    expect(checkSymbol('111Ac~')).toBe(true)
  })
  test('should has symbol # in password to be true', () => {
    expect(checkSymbol('111A##DSs~')).toBe(true)
  })
  test('should has symbol $ in password to be true', () => {
    expect(checkSymbol('111Ac$Exwfc54~')).toBe(true)
  })
  test('should has symbol % in password to be true', () => {
    expect(checkSymbol('111Acasdw65212#%~')).toBe(true)
  })
  test('should has symbol ^ in password to be true', () => {
    expect(checkSymbol('111A^@#asdWASD~')).toBe(true)
  })
  test('should has symbol * in password to be true', () => {
    expect(checkSymbol('111A*asdWASd34c~')).toBe(true)
  })
  test('should has symbol ( in password to be true', () => {
    expect(checkSymbol('111(daWAc~')).toBe(true)
  })
  test('should has symbol ) in password to be true', () => {
    expect(checkSymbol('111)Rwqdas')).toBe(true)
  })
  test('should has symbol - in password to be true', () => {
    expect(checkSymbol('111Ac-iuewr')).toBe(true)
  })
  test('should has symbol _ in password to be true', () => {
    expect(checkSymbol('111EDdfsfe_')).toBe(true)
  })

  test('should has not symbol in password to be false', () => {
    expect(checkSymbol('111Acw')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password bbb@1234 to be true', () => {
    expect(checkPassword('bbb@1234')).toBe(true)
  })
  test('should password abASDc@123!!#$ to be true', () => {
    expect(checkPassword('abASDc@123!!#$')).toBe(true)
  })
  test('should password bbb@123 to be false', () => {
    expect(checkPassword('bbb@123')).toBe(false)
  })
  test('should password 2468@1234 to be false', () => {
    expect(checkPassword('2468@1234')).toBe(false)
  })
  test('should password bbbb@cccc to be false', () => {
    expect(checkPassword('bbbb@cccc')).toBe(false)
  })
  test('should password bbbb1234 to be false', () => {
    expect(checkPassword('bbbb1234')).toBe(false)
  })
})
